import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartWifiComponent } from './smart-wifi.component';

describe('SmartWifiComponent', () => {
  let component: SmartWifiComponent;
  let fixture: ComponentFixture<SmartWifiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartWifiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartWifiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
