import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WasteRealTimeComponent } from './waste-real-time.component';

describe('WasteRealTimeComponent', () => {
  let component: WasteRealTimeComponent;
  let fixture: ComponentFixture<WasteRealTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WasteRealTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WasteRealTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
