import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingRealTimeComponent } from './parking-real-time.component';

describe('ParkingRealTimeComponent', () => {
  let component: ParkingRealTimeComponent;
  let fixture: ComponentFixture<ParkingRealTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkingRealTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingRealTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
