import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartWasteComponent } from './smart-waste.component';

describe('SmartWasteComponent', () => {
  let component: SmartWasteComponent;
  let fixture: ComponentFixture<SmartWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
