import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { MouseEvent } from '@agm/core';

@Component({
  selector: 'app-property-tax',
  templateUrl: './property-tax.component.html',
  styleUrls: ['./property-tax.component.css']
})
export class PropertyTaxComponent implements OnInit {
  single: any[];
  multi: any[];
  private chart: AmChart;
  option:string="Select Year";
  view: any[] = [400, 400];
  zoom: number = 8;
  city:string
  lat:number = 11.0168;
  lng:number = 76.9558;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Cities';
  showYAxisLabel = true;
  yAxisLabel = 'INR';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  mapClicked($event: MouseEvent) {
    // this.markers.push({
    //   lat: $event.coords.lat,
    //   lng: $event.coords.lng,
    //   draggable: true
    // });
  }
  
  markerDragEnd( $event: MouseEvent) {
    console.log('dragEnd',  $event);
  }
  
  markers = [
	  {
		  lat: 11.0168,
      lng: 76.9558,
      label: 'A',
      city: 'Coimbatore',
		  draggable: true
	  },
	  {
      lat: 11.6643,
      lng: 78.1460,
      label: 'B',
      city: 'Salem',
		  draggable: false
	  },
	  {
		  lat: 9.9252,
      lng: 78.1198,
      label: 'C',
      city: 'Madurai',
		  draggable: true
	  }
  ]
  oninitialise(){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 9500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 6700000
       },
       {
         "country": "Oct-Dec",
         "value": 8300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 5500000
       },
      ],
       "titles": [{
         "text": "Coimbatore"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }
  onSelect(event) {
    // debugger
    if(this.option==="2014"){
    if(event.name === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
     "name":"Coimbatore",
      "type": "pie",
      "hideCredits":true,
      "theme": "light",
      "dataProvider": [{
        "country": "Apr-Jun",
        "value": 9500000
      }, 
      {
        "country": "Jul-Sep",
        "value": 6700000
      },
      {
        "country": "Oct-Dec",
        "value": 8300000
      }, 
      {
        "country": "Jan-Mar",
        "value": 5500000
      },
     ],
      "titles": [{
        "text": "Coimbatore"
      }],
      "valueField": "value",
      "titleField": "country",
      "outlineAlpha": 0.4,
      "depth3D": 15,
      // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "angle": 30,
      "export": {
        "enabled": true
      }
    });
  }else if(event.name === "Madurai"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 11500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 7200000
       },
       {
         "country": "Oct-Dec",
         "value": 8300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 6000000
       },
      ],
       "titles": [{
         "text": "Madurai"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Salem"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 11500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 7700000
       },
       {
         "country": "Oct-Dec",
         "value": 8800000
       }, 
       {
         "country": "Jan-Mar",
         "value": 6500000
       },
      ],
       "titles": [{
         "text": "Salem"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Thanjavur"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 12000000
       }, 
       {
         "country": "Jul-Sep",
         "value": 8700000
       },
       {
         "country": "Oct-Dec",
         "value": 9300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 9000000
       },
      ],
       "titles": [{
         "text": "Thanjavur"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Tiruchirapalli"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 12500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 9300000
       },
       {
         "country": "Oct-Dec",
         "value": 10300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 11000000
       },
      ],
       "titles": [{
         "text": "Tiruchirapalli"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Vellore"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 13500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 9800000
       },
       {
         "country": "Oct-Dec",
         "value": 11800000
       }, 
       {
         "country": "Jan-Mar",
         "value": 13000000
       },
      ],
       "titles": [{
         "text": "Vellore"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Tirunelveli"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 15500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 11800000
       },
       {
         "country": "Oct-Dec",
         "value": 12300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 14500000
       },
      ],
       "titles": [{
         "text": "Tirunelveli"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Tiruppur"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 16000000
       }, 
       {
         "country": "Jul-Sep",
         "value": 12300000
       },
       {
         "country": "Oct-Dec",
         "value": 12800000
       }, 
       {
         "country": "Jan-Mar",
         "value": 15000000
       },
      ],
       "titles": [{
         "text": "Tiruppur"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Thoothukudi"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 16500000
       }, 
       {
         "country": "Jul-Sep",
         "value": 12800000
       },
       {
         "country": "Oct-Dec",
         "value": 13300000
       }, 
       {
         "country": "Jan-Mar",
         "value": 15500000
       },
      ],
       "titles": [{
         "text": "Thoothukudi"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }else if(event.name === "Erode"){
    this.chart = this.AmCharts.makeChart( "chartdiv", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Apr-Jun",
         "value": 18000000
       }, 
       {
         "country": "Jul-Sep",
         "value": 13300000
       },
       {
         "country": "Oct-Dec",
         "value": 14800000
       }, 
       {
         "country": "Jan-Mar",
         "value": 16000000
       },
      ],
       "titles": [{
         "text": "Erode"
       }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
  }
}   if(this.option==="2015"){
  if(event.name === "Coimbatore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
   "name":"Coimbatore",
    "type": "pie",
    "hideCredits":true,
    "theme": "light",
    "dataProvider": [{
      "country": "Apr-Jun",
      "value": 10500000
    }, 
    {
      "country": "Jul-Sep",
      "value": 7200000
    },
    {
      "country": "Oct-Dec",
      "value": 9300000
    }, 
    {
      "country": "Jan-Mar",
      "value": 6500000
    },
   ],
    "titles": [{
      "text": "Coimbatore"
    }],
    "valueField": "value",
    "titleField": "country",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": true
    }
  });
}else if(event.name === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 7200000
     },
     {
       "country": "Oct-Dec",
       "value": 8300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 6000000
     },
    ],
     "titles": [{
       "text": "Madurai"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11900000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8200000
     },
     {
       "country": "Oct-Dec",
       "value": 8800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 7000000
     },
    ],
     "titles": [{
       "text": "Salem"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8700000
     },
     {
       "country": "Oct-Dec",
       "value": 8800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 8500000
     },
    ],
     "titles": [{
       "text": "Thanjavur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8300000
     },
     {
       "country": "Oct-Dec",
       "value": 9300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 10000000
     },
    ],
     "titles": [{
       "text": "Tiruchirapalli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 10000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 9300000
     },
     {
       "country": "Oct-Dec",
       "value": 9800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11000000
     },
    ],
     "titles": [{
       "text": "Vellore"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 10500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 10800000
     },
     {
       "country": "Oct-Dec",
       "value": 10300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11500000
     },
    ],
     "titles": [{
       "text": "Tirunelveli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 11300000
     },
     {
       "country": "Oct-Dec",
       "value": 11300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 12000000
     },
    ],
     "titles": [{
       "text": "Tiruppur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 10500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 10800000
     },
     {
       "country": "Oct-Dec",
       "value": 11300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 14000000
     },
    ],
     "titles": [{
       "text": "Thoothukudi"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 15000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 11300000
     },
     {
       "country": "Oct-Dec",
       "value": 11800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11000000
     },
    ],
     "titles": [{
       "text": "Erode"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}   if(this.option==="2016"){
  if(event.name === "Coimbatore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
   "name":"Coimbatore",
    "type": "pie",
    "hideCredits":true,
    "theme": "light",
    "dataProvider": [{
      "country": "Apr-Jun",
      "value": 10500000
    }, 
    {
      "country": "Jul-Sep",
      "value": 7700000
    },
    {
      "country": "Oct-Dec",
      "value": 9300000
    }, 
    {
      "country": "Jan-Mar",
      "value": 6500000
    },
   ],
    "titles": [{
      "text": "Coimbatore"
    }],
    "valueField": "value",
    "titleField": "country",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": true
    }
  });
}else if(event.name === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8200000
     },
     {
       "country": "Oct-Dec",
       "value": 8300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 7000000
     },
    ],
     "titles": [{
       "text": "Madurai"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8200000
     },
     {
       "country": "Oct-Dec",
       "value": 9300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 7000000
     },
    ],
     "titles": [{
       "text": "Salem"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8700000
     },
     {
       "country": "Oct-Dec",
       "value": 9300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 9000000
     },
    ],
     "titles": [{
       "text": "Thanjavur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8200000
     },
     {
       "country": "Oct-Dec",
       "value": 10300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 9000000
     },
    ],
     "titles": [{
       "text": "Tiruchirapalli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8800000
     },
     {
       "country": "Oct-Dec",
       "value": 10800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11000000
     },
    ],
     "titles": [{
       "text": "Vellore"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 10500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 10800000
     },
     {
       "country": "Oct-Dec",
       "value": 10300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11500000
     },
    ],
     "titles": [{
       "text": "Tirunelveli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 15000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 10300000
     },
     {
       "country": "Oct-Dec",
       "value": 11800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 12000000
     },
    ],
     "titles": [{
       "text": "Tiruppur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 11300000
     },
     {
       "country": "Oct-Dec",
       "value": 11300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 11500000
     },
    ],
     "titles": [{
       "text": "Thoothukudi"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 14000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 13300000
     },
     {
       "country": "Oct-Dec",
       "value": 14800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 16000000
     },
    ],
     "titles": [{
       "text": "Erode"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}   if(this.option==="2017"){
  if(event.name === "Coimbatore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
   "name":"Coimbatore",
    "type": "pie",
    "hideCredits":true,
    "theme": "light",
    "dataProvider": [{
      "country": "Apr-Jun",
      "value": 10500000
    }, 
    {
      "country": "Jul-Sep",
      "value": 7200000
    },
    {
      "country": "Oct-Dec",
      "value": 8800000
    }, 
    {
      "country": "Jan-Mar",
      "value": 8500000
    },
   ],
    "titles": [{
      "text": "Coimbatore"
    }],
    "valueField": "value",
    "titleField": "country",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": true
    }
  });
}else if(event.name === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 1150000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8200000
     },
     {
       "country": "Oct-Dec",
       "value": 8300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 8000000
     },
    ],
     "titles": [{
       "text": "Madurai"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 7700000
     },
     {
       "country": "Oct-Dec",
       "value": 9800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 7500000
     },
    ],
     "titles": [{
       "text": "Salem"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8700000
     },
     {
       "country": "Oct-Dec",
       "value": 9300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 9000000
     },
    ],
     "titles": [{
       "text": "Thanjavur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 9300000
     },
     {
       "country": "Oct-Dec",
       "value": 9300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 10000000
     },
    ],
     "titles": [{
       "text": "Tiruchirapalli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 11500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 8800000
     },
     {
       "country": "Oct-Dec",
       "value": 10200000
     }, 
     {
       "country": "Jan-Mar",
       "value": 10000000
     },
    ],
     "titles": [{
       "text": "Vellore"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 10800000
     },
     {
       "country": "Oct-Dec",
       "value": 10300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 12500000
     },
    ],
     "titles": [{
       "text": "Tirunelveli"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 14000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 11000000
     },
     {
       "country": "Oct-Dec",
       "value": 10800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 12000000
     },
    ],
     "titles": [{
       "text": "Tiruppur"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 12500000
     }, 
     {
       "country": "Jul-Sep",
       "value": 11800000
     },
     {
       "country": "Oct-Dec",
       "value": 12300000
     }, 
     {
       "country": "Jan-Mar",
       "value": 13500000
     },
    ],
     "titles": [{
       "text": "Thoothukudi"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}else if(event.name === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Apr-Jun",
       "value": 16000000
     }, 
     {
       "country": "Jul-Sep",
       "value": 12300000
     },
     {
       "country": "Oct-Dec",
       "value": 12800000
     }, 
     {
       "country": "Jan-Mar",
       "value": 13000000 
     },
    ],
     "titles": [{
       "text": "Erode"
     }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}
}
  constructor(private AmCharts: AmChartsService) { }

  ngOnInit() {
    this.oninitialise()
    this.single = [
      {
        "name": "Coimbatore",
        "value": 30000000 
      },{
        "name": "Madurai",
        "value":33000000
      },
      {
        "name": "Salem",
        "value": 35000000
      },
      {
        "name": "Thanjavur",
        "value": 40000000
      },
      
      {
        "name": "Tiruchirapalli",
        "value": 45000000
      },
      {
        "name": "Vellore",
        "value": 50000000
      },
      {
        "name": "Tirunelveli",
        "value": 56000000
      }
      , {
        "name": "Tiruppur",
        "value": 58000000
      } , {
        "name": "Thoothukudi",
        "value": 60000000
      } , {
        "name": "Erode",
        "value": 65000000
      }
      
    ];
    
  }
  selectYear(a){
    // alert(a)
    if(a===null || a === undefined){
      this.single = [
        {
          "name": "Coimbatore",
          "value": 30000000
        },
        {
          "name": "Madurai",
          "value":33000000
        },
        {
          "name": "Salem",
          "value": 35000000
        },
        {
          "name": "Thanjavur",
          "value": 40000000
        },
        
        {
          "name": "Tiruchirapalli",
          "value": 45000000
        },
        {
          "name": "Vellore",
          "value": 50000000
        },
        {
          "name": "Tirunelveli",
          "value": 56000000
        }
        , {
          "name": "Tiruppur",
          "value": 58000000
        } , {
          "name": "Thoothukudi",
          "value": 60000000
        } , {
          "name": "Erode",
          "value": 65000000
        }
        
      ];

    }
    if(a==="2014"){
      this.single = [
        {
          "name": "Coimbatore",
          "value": 30000000
        },
        {
          "name": "Madurai",
          "value":33000000
        },
        {
          "name": "Salem",
          "value": 35000000
        },
        {
          "name": "Thanjavur",
          "value": 40000000
        },
        
        {
          "name": "Tiruchirapalli",
          "value": 45000000
        },
        {
          "name": "Vellore",
          "value": 50000000
        },
        {
          "name": "Tirunelveli",
          "value": 56000000
        }
        , {
          "name": "Tiruppur",
          "value": 58000000
        } , {
          "name": "Thoothukudi",
          "value": 60000000
        } , {
          "name": "Erode",
          "value": 65000000
        }
        
      ];

    } else if(a==="2015"){
      this.single = [
        {
          "name": "Coimbatore",
          "value": 33500000
        },
        {
          "name": "Madurai",
          "value":33000000
        },
        {
          "name": "Salem",
          "value": 36400000
        },
        {
          "name": "Thanjavur",
          "value": 38500000
        },
        
        {
          "name": "Tiruchirapalli",
          "value": 41000000
        },
        {
          "name": "Vellore",
          "value": 43000000
        },
        {
          "name": "Tirunelveli",
          "value": 45600000
        }
        , {
          "name": "Tiruppur",
          "value": 47500000
        } , {
          "name": "Thoothukudi",
          "value": 49000000
        } , {
          "name": "Erode",
          "value": 50000000
        }
        
      ];

    }
    else if(a =="2016"){
      this.single = [
        {
          "name": "Coimbatore",
          "value": 34000000
        },
        {
          "name": "Madurai",
          "value":36000000
        },
        {
          "name": "Salem",
          "value": 37500000
        },
        {
          "name": "Thanjavur",
          "value": 40000000
        },
        
        {
          "name": "Tiruchirapalli",
          "value": 42000000
        },
        {
          "name": "Vellore",
          "value": 44000000
        },
        {
          "name": "Tirunelveli",
          "value": 45000000
        }
        , {
          "name": "Tiruppur",
          "value": 47000000
        } , {
          "name": "Thoothukudi",
          "value": 48500000
        } , {
          "name": "Erode",
          "value": 61000000
        }
        
      ];
    }else if(a=="2017"){
      this.single = [
        {
          "name": "Coimbatore",
          "value": 35000000
        },
        {
          "name": "Madurai",
          "value":36000000
        },
        {
          "name": "Salem",
          "value": 38000000
        },
        {
          "name": "Thanjavur",
          "value": 40000000
        },
        
        {
          "name": "Tiruchirapalli",
          "value": 42000000
        },
        {
          "name": "Vellore",
          "value": 44400000
        },
        {
          "name": "Tirunelveli",
          "value": 47500000
        }
        , {
          "name": "Tiruppur",
          "value": 49700000
        } , {
          "name": "Thoothukudi",
          "value": 52000000
        } , {
          "name": "Erode",
          "value": 57000000
        }
        
      ];
    }
  }
}
