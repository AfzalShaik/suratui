import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTransportComponent } from './smart-transport.component';

describe('SmartTransportComponent', () => {
  let component: SmartTransportComponent;
  let fixture: ComponentFixture<SmartTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
