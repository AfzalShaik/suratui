import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirPurityComponent } from './air-purity.component';

describe('AirPurityComponent', () => {
  let component: AirPurityComponent;
  let fixture: ComponentFixture<AirPurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirPurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirPurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
