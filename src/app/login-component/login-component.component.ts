import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../guards';
import { CookieService } from 'angular2-cookie/core';
import { GraphsService } from '../../services/graphs/graphs.service';
@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css'],
  providers : [GraphsService]
})
export class LoginComponentComponent implements OnInit {

  login: any = {};
  loginResponse: any;
  token: any;
  constructor(private authGuard:AuthGuard, private cookieService: CookieService, private GS: GraphsService) { }

  ngOnInit() {
  }

  loginForm(){
    this.authGuard.postLogin(this.login).subscribe(data=> {
      this.loginResponse = data;
        this.token = this.loginResponse.id;
        this.authGuard.finishAuthentication(this.token, this.loginResponse);
        this.cookieService.putObject('loginResponse', this.loginResponse);
    });
  }
}
