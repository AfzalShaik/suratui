import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateAllComponent } from './state-all.component';

describe('StateAllComponent', () => {
  let component: StateAllComponent;
  let fixture: ComponentFixture<StateAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
