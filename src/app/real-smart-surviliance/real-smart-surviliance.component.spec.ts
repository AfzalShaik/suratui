import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealSmartSurvilianceComponent } from './real-smart-surviliance.component';

describe('RealSmartSurvilianceComponent', () => {
  let component: RealSmartSurvilianceComponent;
  let fixture: ComponentFixture<RealSmartSurvilianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealSmartSurvilianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealSmartSurvilianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
