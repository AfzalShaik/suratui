import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterScadaComponent } from './water-scada.component';

describe('WaterScadaComponent', () => {
  let component: WaterScadaComponent;
  let fixture: ComponentFixture<WaterScadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterScadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterScadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
