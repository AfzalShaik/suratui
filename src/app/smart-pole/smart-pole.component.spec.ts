import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartPoleComponent } from './smart-pole.component';

describe('SmartPoleComponent', () => {
  let component: SmartPoleComponent;
  let fixture: ComponentFixture<SmartPoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartPoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartPoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
