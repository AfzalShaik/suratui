import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { AmChartsService,AmChart } from '@amcharts/amcharts3-angular';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard-screen.component.html',
    styleUrls: ['./dashboard-screen.component.css'],
    providers: [GraphsService]
  })
  export class dashboardScreenComponent implements OnInit {
    private chart: AmChart;
    options: any;
    Data: any = []
  status: any = {};
  zoom: number = 14;
  mapdata:any;
  lat: number  
  lng: number 
  markers:any=[]
    constructor(private serviceAssert:GetDigitalAssetType, private router: Router,private AmCharts: AmChartsService) { }
    mapClicked($event: MouseEvent) {
        console.log($event)
        this.markers.push({
          lat: $event.coords.lat,
          lng: $event.coords.lng,
          draggable: true
        });
      }
    ngOnInit() {
        this.allcities()
        this.smartParking()
        this.environmental()
    }
    environmental(){
        this.lat =13.0817597;
        this.lng =80.2694349
        this.markers = [
          {
            lat: 13.083933374053084,
            lng: 80.2670826026939,
            iconUrl:'../assets/images/fires.gif',
            draggable: false
          },
          {
            lat: 13.077836521507338,
            lng:80.27402411307139,
            iconUrl:'../assets/images/fires.gif',
            draggable: false
          }
        ]
      }
    
    smartParking(){
        this.options = {
            title: { text: '' },
            xAxis: {
              categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
              max:6
          },
          yAxis: {
            title: { text: 'INR' }
          },
          tooltip:{
            borderWidth: 0,
            shadow: false,
            style: {
              padding: 0
            },
                    formatter:function(){
                    return 'INR '+ this.y;
                    }
                },
                  series: [{
                      name: 'Time Line',
                      data: [2122579, 2410257,  2388414, 2256087, 1958543, 2366365, 1985927],
                      tooltip: {
                          valueDecimals: 2
                      },
                      allowPointSelect: true
                  }]
              };
    }
    allcities(){
        this.chart = this.AmCharts.makeChart("chartdiv", {
          "type": "serial",
          "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
            "useGraphSettings": true,
            "markerSize": 10
            },
            "dataProvider": [{
                "year": "Coimbatore",
                "europe": 270,
                "namerica": 75,
                "asia": 85,
                "lamerica":70,
                "meast": 500,
            }, {
                "year": "	Madurai",
                "europe": 370,
                "namerica": 70,
                "asia": 105,
                "lamerica":55,
                "meast": 600,
            }, {
                "year": "Salem",
                "europe": 270,
                "namerica": 65,
                "asia": 85,
                "lamerica":75,
                "meast": 500,
            }, {
                "year": "Thanjavur",
                "europe": 380,
                "namerica": 60,
                "asia": 125,
                "lamerica":35,
                "meast": 600,
          }, {
            "year": "Tiruchirapalli",
                "europe": 455,
                "namerica":175,
                "asia": 200,
                "lamerica":70,
                "meast": 900,
        }, 
        {
          "year": "Tirunelveli",
          "europe": 228,
            "namerica":145,
            "asia": 71,
            "lamerica":0,
            "meast": 444,
        },{
          "year": "Vellore",
          "europe": 130,
          "namerica":35,
          "asia": 35,
          "lamerica": 30,
          "meast": 230,
      }, {
        "year": "Tiruppur",
        "europe": 365,
        "namerica":145,
        "asia": 175,
        "lamerica":81,
        "meast": 766,
    }, {
      "year": "Thoothukudi",
      "europe": 228,
        "namerica":145,
        "asia": 71,
        "lamerica":0,
        "meast": 444,
    }, {
      "year": "Erode",
      "europe":432,
      "namerica":0,
      "asia": 208,
      "lamerica": 160,
      "meast": 800,
    }
          ],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "gridAlpha": 0
            }],
            "titles": [{
              "text": "Digital Assets in all cities"
            }],
            "graphs": [
             
              {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Working",
                "type": "column",
                "color": "#000000",
                "fillColors": "#5ca026",
                "valueField": "europe"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Stopped",
                "type": "column",
            "color": "#000000",
            "fillColors":"#e60000",
                "valueField": "namerica"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Maintanance",
                "type": "column",
                "color": "#000000",
                "fillColors":"#b87b06",
                "valueField": "asia"
            }, 
            {
              "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
              "fillAlphas": 0.8,
              "labelText": "[[value]]",
              "lineAlpha": 0.3,
              "title": "Expected Maintenance",
              "type": "column",
              "color": "#000000",
              "fillColors":"#ffa500",
              "valueField": "lamerica"
          }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
              "enabled": true
             }
        })
        }
  }