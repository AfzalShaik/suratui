import { Component, OnInit, NgModule } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { Time } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GetDigitalAssetType } from '../digital-asset-dashboard/digital-asset-dashboard.services';
import { toUnicode } from 'punycode';
declare var $: any;

@Component({
  selector: 'app-real-traffic',
  templateUrl: './real-traffic.component.html',
  styleUrls: ['./real-traffic.component.css'],
  providers: [GraphsService, GetDigitalAssetType]
})
export class RealTrafficComponent implements OnInit {
  zoom1: number = 12;
  lat: any = 13.0480438;
  lng: any = 80.1389196;
  paramData: any;
  city: any;
  info1: any;
  goodVar: boolean;
  manuallight: any;
  host: any;
  info: any;
  location: any;
  manual: any;
  light: any;
  health: any;
  healthValue: string;
  mode: any;
  cycle: any;
  delay: any;
  trafficObj: { "citizenId": string; "citizenName": string; "citizenMobileNumber": string; "areaName": string; "department": string; "description": string; "alertType": string; "city": string; "read": string; "count": number; };
  constructor(private GS: GraphsService, private route: ActivatedRoute, private serviceAssert: GetDigitalAssetType, private router: Router) { 

    // setInterval(() => { this.getTrafficData(); }, 1000 * 1 * 60);
  }

  ngOnInit() {
    this.getTrafficData();
    this.paramData = this.route.params.subscribe(params => {
      this.city = params.id;
      this.info1 = this.city;
    });
  }
  getTrafficData() {
    this.GS.getTrafficData().subscribe(data => {
      // debugger;
      let len = data.data.length;
      let obj = {};
      len = len-1;
      obj = data.data[len];
      // console.log('objjjj ', obj);
      let trafficArr = [];
      trafficArr.push(obj);
      let iconUrl = '';
      if (trafficArr[0].h <= 7) {
        iconUrl = '../assets/images/green.png';
        this.healthValue = 'No Traffic';
      } else {
        iconUrl = '../assets/images/traffic.gif';
        this.healthValue = 'Heavy Traffic';
      }
      
      this.trafficMarkers = [
        {
          lat:  13.090252808991249,
          lng:  80.16043210024327,
          label: '',
          draggable: false,
          iconUrl: iconUrl,
          info: '',
          type: '',
          traffic: trafficArr
        }
      ]

    });
  }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event.coords);
    
  }
  clickedMarker(label: any,infoWindow, gm) {
    this.goodVar = true;
    this.health = label.traffic[0].h;
    this.mode = label.traffic[0].m;
    this.cycle = label.traffic[0].ct;
    this.delay = label.traffic[0].it;
    let icon = '';
    // debugger;
    if (label.iconUrl == '../assets/images/green.png') {
    } else if (label.iconUrl == '../assets/images/traffic.gif') {
      this.trafficObj =  {
        "citizenId": "5af03877aa9cae2e42c1e62b",
        "citizenName": "sairam",
        "citizenMobileNumber": "7799849440",
        "areaName": "R S Puram",
        "department": "Police",
        "description": "Heavy ttrafic in my area",
        "alertType": "City Traffic",
        "city": "Coimbatore",
        "read": "No",
        "count": 1
      }
      this.GS.postAlert(this.trafficObj).subscribe(trafficData => {

      });
      // this.router.navigate(['/dashboard/incident/CoimbatoreTraffic/']);
    } else {

    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
   
  }
  mouseOver($event: MouseEvent, label: any, infoWindow, gm) {
    this.goodVar = true;
    this.health = label.traffic[0].h;
    this.mode = label.traffic[0].m;
    this.cycle = label.traffic[0].ct;
    this.delay = label.traffic[0].it;
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  trafficMarkers: trafficMarker[] = [
    {
      lat:  13.090252808991249,
      lng:  80.16043210024327,
      label: '',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      traffic: []
    }
  ]
}
interface trafficMarker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: any;
  type: string;
  traffic: any[];
}